terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}


// Create a new instance
resource "yandex_compute_instance" "vm" {
  count = var.numbers
  
  name = "diploma-${count.index + 1}"
  resources {
    cores  = 2
    memory = 2    
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fte6bebi857ortlja"
      size = 10
    }    
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/yandex-terraform1.pub")}"
  }
  
  // клонирование репозитория
  provisioner "local-exec" {
    command = "%{ if count.index == 0 }git submodule update%{ else }echo 'репозиторий уже обновлён'%{ endif }"
  }

  provisioner "file" {
    source      = "scripts/create.sh"
    destination = "/tmp/create.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = self.network_interface[0].nat_ip_address
    }
  }
  provisioner "file" {
    source      = "skillbox-diploma"
    destination = "skillbox-diploma"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = self.network_interface[0].nat_ip_address
    }
  }
  
  # Могут быть ошибки чтения файла, исправить sed -i -e 's/\r$//' create.sh
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/create.sh",
      "/tmp/create.sh",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = self.network_interface[0].nat_ip_address
    }
  }

  provisioner "remote-exec" {
    inline = [
      "cd skillbox-diploma",
      "sudo docker-compose up -d --build",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/yandex-terraform1")}"
      host        = self.network_interface[0].nat_ip_address
    }
  }

  lifecycle {
    create_before_destroy = true
  }

}

resource "yandex_vpc_network" "network-1" {
  name = "network-diploma"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-diploma"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_lb_target_group" "diploma" {
  name       = "diploma-target-group"
  depends_on = [yandex_compute_instance.vm]

  dynamic "target" {
    for_each = yandex_compute_instance.vm
    content {
      subnet_id = yandex_vpc_subnet.subnet-1.id
      address   = target.value.network_interface.0.ip_address
    }
  }
}

resource "yandex_lb_network_load_balancer" "diploma" {
  name = "diploma-network-load-balancer"
  listener {
    name = "diploma-listener"
	  port = 80
    target_port = 8080
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  attached_target_group {
    target_group_id = yandex_lb_target_group.diploma.id
    healthcheck {
      name = "test-my-app"
        http_options {
          port = 8080
          path = "/health"
        }
    }
  }
}

// https://cloud.yandex.ru/docs/dns/concepts/dns-zone
// https://cloud.yandex.ru/docs/dns/quickstart
resource "yandex_dns_zone" "dns_domain" {
  name   = replace(var.dns_domain, ".", "-")
  zone   = join("", [var.dns_domain, "."])

  labels = {
    label1 = "test-public"
  }

  public           = true
  private_networks = [yandex_vpc_network.network-1.id]
}

resource "yandex_dns_recordset" "diploma_dns_domain_www" {
  zone_id = yandex_dns_zone.dns_domain.id
  name    = "www"
  type    = "A"
  ttl     = 600
  data    = [yandex_lb_network_load_balancer.diploma.listener.*.external_address_spec[0].*.address[0]]
}

resource "yandex_dns_recordset" "diploma_dns_domain" {
  zone_id = yandex_dns_zone.dns_domain.id
  name    = "@"
  type    = "A"
  ttl     = 600
  data    = [yandex_lb_network_load_balancer.diploma.listener.*.external_address_spec[0].*.address[0]]
}
