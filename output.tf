output "internal_ip_address_node" {
  value = yandex_compute_instance.vm.*.network_interface.0.ip_address
}

output "external_ip_address_node" {
  value = yandex_compute_instance.vm.*.network_interface.0.nat_ip_address
}

output "external_ip_address_load_balancer" {
  value = yandex_lb_network_load_balancer.diploma.listener.*.external_address_spec[0].*.address[0]
}