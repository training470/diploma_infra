## Инициализация terraform
### Настройка
Используем облачного провайдера VK Cloud. [Инструкция](https://mcs.mail.ru/docs/ru/additionals/terraform/terraform-provider-config) по настройке провайдера.  
Чувствительные данные должны быть указаны в *secret-variables.tfvars*. Шаблон файла в *secret-variables.tfvars.dist*.  
```commandline
terraform init
terraform apply -var-file=secret-variables.tfvars
terraform destroy -var-file=secret-variables.tfvars
ssh -i ~/.ssh/yandex-terraform1 ubuntu@51.250.82.164
```
