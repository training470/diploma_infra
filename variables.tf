variable "yandex_token" {
    description = "token for yandex cloud"
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "yandex cloud id"
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "yandex folder id"
    type = string
}
variable "numbers" {
    description = "count of instances"
    type = number
}
variable "dns_domain" {
    description = "domain name of load balancer"
    type = string
}